﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decay : MonoBehaviour {

    public int timeToDie = 5;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, timeToDie);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
