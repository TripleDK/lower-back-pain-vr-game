﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TotalScore : MonoBehaviour {

    public static int totalScore;
    public int visibleScore;
    public float updateSpeed = 0.5f;
    public TextMeshPro text;
    public AudioClip tick;

	// Use this for initialization
	void Start () {
        text = GetComponent<TextMeshPro>();
        visibleScore = 0;
        text.text = "SCORE: 0";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    public void AddScore(int score)
    {
		  StartCoroutine(AddingScore(score));
        StartCoroutine(BlinkEffect());
    }

    IEnumerator BlinkEffect()
    {
        float timer = 0;
        while(timer < updateSpeed)
        {
            timer += Time.deltaTime;
            if (timer < updateSpeed / 2)
            {
                text.color = new Color(1 - timer / updateSpeed, 1, 1 - timer / updateSpeed);
                transform.localScale = new Vector3(1, 1, 1) * (1 + (1 - timer / updateSpeed)/2);
            }
            else
            {
                text.color = new Color(timer / updateSpeed, 1, timer / updateSpeed);
                transform.localScale = new Vector3(1, 1, 1) * ((1 + ((updateSpeed-timer)/2) / updateSpeed));
            }
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator AddingScore(int score)
    {
        float timer = 0;
        int initScore = visibleScore;
      
        while(timer < updateSpeed)
        {
            timer += Time.deltaTime;
            visibleScore += (int)Mathf.Round(score * Time.deltaTime/updateSpeed);
            text.text = "SCORE: "+visibleScore;
             AudioSource.PlayClipAtPoint(tick, transform.position);

            yield return new WaitForEndOfFrame();
        }
        visibleScore = initScore + score;
        text.text = "SCORE: " + visibleScore;
    }
    public void AddToTotalScore()
    {
		StartCoroutine (CoAddToTotalScore ());
    }

	IEnumerator CoAddToTotalScore() {
		yield return new WaitForSeconds (updateSpeed * 4f);
		totalScore += visibleScore;
		visibleScore = 0;
		yield return new WaitForSeconds (2);
		text.text = "SCORE: " + visibleScore;
	}
}
