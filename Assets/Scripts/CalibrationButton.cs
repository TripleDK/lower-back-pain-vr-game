﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalibrationButton : MonoBehaviour
{

	public Transform playerHead, pointer;
	public PoseTracker posTracker;
	public AudioClip confirmSound;
	public float lookTime;
	float lookingTime;
	Material gazeMaterial;
	Material backgroundMat;
	Transform currentSelected;

	// Use this for initialization
	void Start ()
	{

		posTracker = playerHead.gameObject.GetComponent<PoseTracker> ();
		backgroundMat = GetComponent<Renderer> ().material;
		gazeMaterial = transform.FindChild ("Plane (2)").GetComponent<Renderer> ().material;
		lookingTime = 0;
		gazeMaterial.SetColor ("_EmissionColor", Color.black);
		backgroundMat.SetColor ("_EmissionColor", Color.black);
		currentSelected = null;
	}
	
	// Update is called once per frame
	void Update ()
	{
		pointer.position = playerHead.transform.position + playerHead.transform.forward * 15;
		pointer.LookAt (playerHead.transform);
		Ray playerForwardRay = new Ray (playerHead.position, playerHead.forward);
		RaycastHit hit;
		if (Physics.Raycast (playerForwardRay, out hit, (transform.position - playerHead.position).magnitude * 1.5f)) {
			if (hit.transform == this.transform) {
				if (currentSelected == null) {
					transform.localScale *= 1.1f;
					currentSelected = this.transform;
					backgroundMat.SetColor ("_EmissionColor", Color.yellow);
				}
				lookingTime += Time.deltaTime;
			} else {
				if (currentSelected == transform) {
					transform.localScale /= 1.1f;
					backgroundMat.SetColor ("_EmissionColor", Color.black);
				}
				currentSelected = null;
				lookingTime -= Time.deltaTime;
			}
		} else {
			if (currentSelected == transform) {
				transform.localScale /= 1.1f;
				backgroundMat.SetColor ("_EmissionColor", Color.black);
			}
			currentSelected = null;
			lookingTime -= Time.deltaTime;
		}
		if (lookingTime < 0)
			lookingTime = 0;
		gazeMaterial.SetColor ("_EmissionColor", Color.yellow * (lookingTime / lookTime));
		if (lookingTime >= lookTime) {
			lookingTime = 0;
			Debug.Log ("Witnessed!");
			gazeMaterial.SetColor ("_EmissionColor", Color.black);
			backgroundMat.SetColor ("_EmissionColor", Color.black);
			AudioSource.PlayClipAtPoint (confirmSound, playerHead.position);
			currentSelected = null;
			posTracker.StartCoroutine (posTracker.Calibrate ());

		}

	}
}
