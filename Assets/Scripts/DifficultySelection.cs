﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultySelection : MonoBehaviour
{

	public Transform easier, same, harder;
	public Transform player;
	public Transform pointer;
	public PoseTracker posTracker;
	public EnemySpawner enSpa;
    public AudioClip confirmSound;
	public List<Transform> selections = new List<Transform> ();
    public float timer = 2;
    float timeLooked;

	Color currentSelectedColor;
	Transform currentSelected;

	// Use this for initialization
	void Start ()
	{
		currentSelected = same;
		currentSelectedColor = currentSelected.GetComponent<MeshRenderer> ().material.color;
		currentSelected.GetComponent<MeshRenderer> ().material.color = Color.yellow;
		currentSelected.GetComponent<MeshRenderer> ().material.SetColor ("_EmissionColor", Color.yellow);
		currentSelected.localScale *= 1.1f;
		selections.Add (easier);
		selections.Add (same);
		selections.Add (harder);
        timeLooked = 0;
	}

	// Update is called once per frame
	void Update ()
	{
		Ray playerForwardRay = new Ray (player.transform.position, player.transform.forward);
		RaycastHit hit;
		if (Physics.Raycast (playerForwardRay, out hit, (transform.position - player.transform.position).magnitude * 1.5f)) {
			if (selections.Contains (hit.transform)) {
				if (hit.transform == currentSelected) {

				} else {
					if (currentSelected != null) {
						currentSelected.GetComponent<MeshRenderer> ().material.color = currentSelectedColor;
						currentSelected.GetComponent<MeshRenderer> ().material.SetColor ("_EmissionColor", currentSelectedColor);
						currentSelected.localScale /= 1.1f;
					}
					currentSelected = hit.transform;
					currentSelected.localScale *= 1.1f;
					currentSelectedColor = currentSelected.GetComponent<MeshRenderer> ().material.color;
                 

				}
			} else {
				if (currentSelected != null) {
					currentSelected.GetComponent<MeshRenderer> ().material.color = currentSelectedColor;
					currentSelected.GetComponent<MeshRenderer> ().material.SetColor ("_EmissionColor", currentSelectedColor);
					currentSelected.localScale /= 1.1f;
					currentSelected = null;
				}
			}
		} else {
			if (currentSelected != null) {
				currentSelected.GetComponent<MeshRenderer> ().material.color = currentSelectedColor;
				currentSelected.GetComponent<MeshRenderer> ().material.SetColor ("_EmissionColor", currentSelectedColor);
				currentSelected.localScale /= 1.1f;
				currentSelected = null;
			}
		}

      
        if (currentSelected != null)
        {
            timeLooked += Time.deltaTime;
         
            currentSelected.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.yellow * (timeLooked / timer));
        }
        else
        {
            timeLooked = 0;
        }
        pointer.position = player.transform.position + player.transform.forward * 15;
		pointer.LookAt (player.transform);
	//	if (Input.GetKeyDown ("joystick button 2") || Input.GetKeyDown ("joystick button 0") || Input.GetKeyDown ("joystick button 8") || Input.GetKeyDown ("joystick button 9")) { //Either controller's menu button
		if(timeLooked >= timer) {
            timeLooked = 0;
            AudioSource.PlayClipAtPoint(confirmSound, transform.position);
            if (posTracker.calibrationProgress > 2) {
				Debug.Log ("Trying to select difficulty!");
                if (currentSelected == easier)
                {
                    posTracker.leftAngleMax -= 10;
                    posTracker.rightAngleMax -= 10;
                }
                else if (currentSelected == same)
                {

                }
                else if (currentSelected == harder)
                {
                    posTracker.leftAngleMax += 10;
                    posTracker.rightAngleMax += 10;
                }
                else
                {
                    return;
                }
				posTracker.calibrationProgress = 3;
				enSpa.StartGame ();
				gameObject.SetActive (false);
			}
		}
	}
}
