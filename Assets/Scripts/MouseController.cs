﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour {

	public float rotationSpeed = 1;
	public PoseTracker posTrack;
	CursorLockMode cursorMode;

	// Use this for initialization
	void Start () {
		posTrack = GetComponent<PoseTracker> ();
		Cursor.lockState = CursorLockMode.Locked;
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround (transform.position, Vector3.up, Input.GetAxis ("Mouse X"));
		transform.RotateAround (transform.position, -Vector3.right, Input.GetAxis ("Mouse Y"));
		transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y, 0);
	}



}
