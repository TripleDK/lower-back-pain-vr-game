﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyBehavior : MonoBehaviour {

	[HideInInspector]
    public GameObject player;
    [HideInInspector]
    public PoseTracker posTracker;
    [HideInInspector]
    public EnemySpawner enSpa;
	public GameObject explosion1, explosion2, flyingScore;
	public AudioClip deathSound, connectSound;
	public float speed = 5f;
	public float deadForce = 3f;
	public bool isDead;
    [HideInInspector]
    public Rigidbody rigid;
    public AudioClip hitSound;
    public List<Mesh> meteorMesh = new List<Mesh>();
    MeshRenderer rend;

    GameObject tempExplosion1, tempExplosion2;
    // Use this for initialization
    void Start() {

        posTracker = player.GetComponent<PoseTracker>();
        enSpa = player.GetComponent<EnemySpawner>();
        rigid = GetComponent<Rigidbody>();
        rend = GetComponent<MeshRenderer>();
        isDead = false;
        GetComponent<MeshFilter>().mesh = meteorMesh[Random.Range(0, meteorMesh.Count)];
        GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.red);
        StartCoroutine(WarpIn());
	}
	
	// Update is called once per frame
	void Update () {
		if (!isDead && EnemySpawner.gameOn && EnemySpawner.gamePaused == false) {
			transform.position = Vector3.MoveTowards (transform.position, posTracker.shield0Pos, speed * Time.deltaTime);
         
            if (transform.position == posTracker.shield0Pos)
            {
                Hit();
            }
			rigid.velocity = Vector3.zero;
		}
	}

	void OnCollisionEnter (Collision col) {

        if (!isDead)
        {
            if (col.gameObject.tag == "Player")
            {
                Hit();
            }
            if (col.gameObject.name == "Shield")
            {
                //	Destroy (gameObject);
                SpawnPoints();
                Die(col.transform.position);
            }
        }
	}

    void OnTriggerEnter(Collider col)
    {

        if (!isDead)
        {
            if (col.gameObject.tag == "Player")
            {
                StartCoroutine(player.GetComponent<PoseTracker>().ApplyDamage(20));
                posTracker.enemyDamage += 20;
                AudioSource.PlayClipAtPoint(hitSound, transform.position);
                Destroy(gameObject);
            }
          
        }
    }
    public void Die(Vector3 otherPos)
    {
      
        rigid.AddForce((transform.position - otherPos).normalized * deadForce, ForceMode.Impulse);
        rigid.useGravity = true;
        isDead = true;
        tempExplosion1 = Instantiate(explosion1, transform.position, Quaternion.identity);
        GetComponent<MeshRenderer>().material.color = new Color(1, 0, 0, 0.2f);
        GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.black);
        AudioSource.PlayClipAtPoint(hitSound, transform.position);
        StartCoroutine(DeathDelay());

    }

    public void SpawnPoints()
    {
        GameObject tempScore = (GameObject)Instantiate(flyingScore, transform.position, Quaternion.identity);
        tempScore.transform.LookAt(posTracker.transform.position);
        tempScore.transform.Rotate(new Vector3(0, 180, 0));
        tempScore.GetComponent<FlyingScore>().totalScore = posTracker.score.transform;
        tempScore.GetComponent<FlyingScore>().score = 10 * PoseTracker.multiplier;
        tempScore.GetComponent<TextMeshPro>().text = "" + (10 * PoseTracker.multiplier);
    }

    public void Hit() 
    {
        
        posTracker.enemyDamage += 20;
        StartCoroutine(player.GetComponent<PoseTracker>().ApplyDamage(20));
        tempExplosion1 = Instantiate(explosion2, transform.position, Quaternion.identity);
        Destroy(tempExplosion1, 2);
        AudioSource.PlayClipAtPoint(deathSound, transform.position);
		AudioSource.PlayClipAtPoint (connectSound, transform.position);
        enSpa.currentDifficultySpeed *= 0.9f;
        enSpa.allEnemies.Remove(gameObject);
        foreach (GameObject enemy in enSpa.allEnemies)
        {
            if (enemy != null)
            {
                enemy.GetComponent<EnemyBehavior>().speed = enSpa.currentDifficultySpeed;
                enemy.GetComponent<EnemyBehavior>().Die(enemy.transform.position+Vector3.up);
            }
        }
        Destroy(gameObject);
    }

    

	IEnumerator DeathDelay () {
        GetComponent<SphereCollider>().enabled = false;
        yield return new WaitForSeconds(1);
        tempExplosion2 = Instantiate(explosion2, transform.position, Quaternion.identity);
        AudioSource.PlayClipAtPoint(deathSound, transform.position);
        rend.enabled = false;
        enSpa.allEnemies.Remove(gameObject);
        yield return new WaitForSeconds (3);
        Destroy(tempExplosion1);
        Destroy(tempExplosion2);
		Destroy (gameObject);
	}

    IEnumerator WarpIn()
    {
        float timePassed = -2;
        while (timePassed < 2)
        {
            yield return new WaitForFixedUpdate();
            timePassed += Time.fixedDeltaTime*10;
            rend.material.SetFloat("_YClip", timePassed);
        }
    }
}
