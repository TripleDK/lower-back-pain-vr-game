﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingScore : MonoBehaviour {

    public Transform totalScore;
    public float initSpeed = 0.01f;
    public float acceleration = 2;
    public float maxSpeed = 50;
    public int score;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate((totalScore.position-transform.position).normalized * initSpeed, Space.World);
        Debug.DrawLine(transform.position, transform.position + (totalScore.position - transform.position).normalized * initSpeed, Color.green);
        initSpeed *= 1+acceleration * Time.deltaTime;
        if(initSpeed >= maxSpeed)
        {
            acceleration = 0;
            initSpeed = 0;
            transform.position = totalScore.position;
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform == totalScore)
        {
            totalScore.GetComponent<TotalScore>().AddScore(score);

            Destroy(gameObject);
        }
    }
}
