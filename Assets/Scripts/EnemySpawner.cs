﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class EnemySpawner : MonoBehaviour
{
	public static bool gameOn, gamePaused;
	public PoseTracker posTracker;
	public DifficultySelection diffScreen;
	public GameObject enemy, enemyPointerLeft, enemyPointerRight;
	public TotalScore score;
	public GameObject flyingScore;
	public static int currentLevel = 1;
	public float levelTimeLeft;
	public float level1Time;
	public float spawnRate;
	public float difficultyIncrease = 1.1f;
	public float currentDifficultyAngle, currentDifficultySpeed;
	public float spawnTimeLeft;
	public GameObject highscoreScreen;
	public TextMeshPro gameInfo, hiddenScore, currentTotalAngle;
	public AudioClip spawnSound, gameOverSound, winSound;
	public List<GameObject> allEnemies = new List<GameObject> ();
	public AnimationCurve spawnCurve;
	public float levelMaxTime;
	Camera mainCam;

	// Use this for initialization
	void Start ()
	{
		posTracker = GetComponent<PoseTracker> ();
		mainCam = GetComponent<Camera> ();
		gameOn = false;
		gamePaused = false;
		if (!File.Exists (Application.dataPath + "/scores/scores.txt")) {
			Debug.Log ("Creating high score file");
			File.WriteAllText (Application.dataPath + "/scores/scores.txt", "HIGH SCORES: \n\n");
		}
		highscoreScreen.SetActive (false);
	}


	// Update is called once per frame
	void Update ()
	{
		Vector3 leftBoundary, rightBoundary;
		leftBoundary = Quaternion.AngleAxis (-posTracker.leftAngleMax, Vector3.up) * posTracker.forward0;
		rightBoundary = Quaternion.AngleAxis (posTracker.rightAngleMax, Vector3.up) * posTracker.forward0;
		Debug.DrawLine (posTracker.waist.position, posTracker.waist.position + leftBoundary, Color.blue);
		Debug.DrawLine (posTracker.waist.position, posTracker.waist.position + rightBoundary, Color.red);
		hiddenScore.text = "Total score: " + TotalScore.totalScore;
		if (Input.GetKeyDown (KeyCode.Return)) {
			StartGame ();
		}

		if (gameOn) {
			spawnTimeLeft -= Time.deltaTime;
			levelTimeLeft -= Time.deltaTime;
			gameInfo.text = "Stability: " + (int)posTracker.health + "\nProgress: " + Mathf.Max ((int)(100 * (1 - levelTimeLeft / levelMaxTime)), 0) + "%";

			bool leftEnemy = false;
			bool rightEnemy = false;
			for (int i = 0; i < allEnemies.Count; i++) {
				//    Debug.Log("Current enemi: " + enemy.name);
				Vector3 cameraForward = Vector3.ProjectOnPlane (transform.forward, Vector3.up);
				Vector3 normal = Vector3.Cross (cameraForward, allEnemies [i].transform.position - posTracker.chest0Pos);
				Debug.DrawLine (transform.position, transform.position + normal, Color.yellow);
				/*      if (screenPos.z > 0 && screenPos.x >= 0 && screenPos.x <= 1)
                {
                    //In screen
                }
                else
                {*/
				if (normal.y < -18) {
					leftEnemy = true;
				} else if (normal.y > 18) {
					rightEnemy = true;
				}
                
			}

			if (leftEnemy) {
				enemyPointerLeft.SetActive (true);
			} else {
				enemyPointerLeft.SetActive (false);
			}
			if (rightEnemy) {
				enemyPointerRight.SetActive (true);
			} else {
				enemyPointerRight.SetActive (false);
			}


			// Debug.Log("Progress: " + Mathf.Min((int)(100 * (1 - levelTimeLeft / levelMaxTime)), 0) + "%");

			if (levelTimeLeft <= 10 && levelTimeLeft >= 9) {
				DialogueManager.source.PlayAlmostWin ();
			
			}

			if (levelTimeLeft <= 0) {
				AudioSource.PlayClipAtPoint (winSound, transform.position);
				if (currentLevel < 4)
					DialogueManager.source.PlayWin ();
				else  if (currentLevel == 4) {
					DialogueManager.source.PlayFinalWin ();
				}
				EndGame ();
				NextLevel ();
			}
			if (spawnTimeLeft <= 0) {
				SpawnEnemy ();
				spawnRate /= difficultyIncrease;
				currentDifficultySpeed *= difficultyIncrease;
				spawnTimeLeft = spawnRate;

			}
		}


		//posTracker.headJoint.eulerAngles = posTracker.head.transform.eulerAngles;
		//  posTracker.headJoint.Rotate(posTracker.headJoint.right, 90);
		//    posTracker.waistJoint.LookAt(posTracker.waistJoint.position+posTracker.waist.transform.up);
		// posTracker.waistJoint.Rotate(posTracker.waistJoint.right, 90);
		//   posTracker.chestJoint.LookAt(posTracker.chestJoint.position + posTracker.chest.transform.forward);// = posTracker.chest.transform.rotation * Quaternion.AngleAxis(-90, posTracker.chest.transform.forward) ;
		//    posTracker.chestJoint.Rotate(posTracker.chestJoint.forward, -90);


	}

	public void StartGame ()
	{
		gameOn = true;
		posTracker.score.transform.GetChild (1).GetComponent<TextMeshPro> ().text = "Stage: " + currentLevel;
		posTracker.startText.gameObject.SetActive (false);
        posTracker.calibrationProgress = 4;
        currentDifficultyAngle = 0.5f;
		currentDifficultySpeed = 1;
		Debug.Log ("Game start level: " + currentLevel);
		switch (currentLevel) {
		case 1:
			spawnRate = 2;
			levelTimeLeft = level1Time;
			difficultyIncrease = 1.03f;
			break;
		case 2:
			spawnRate = 1.5f;
			levelTimeLeft = 30;
			difficultyIncrease = 1.03f;
			break;

		case 3:
			spawnRate = 1.3f;
			levelTimeLeft = 25;
			difficultyIncrease = 1.03f;
			break;
		case 4:
			spawnRate = 1;
			levelTimeLeft = 30;
			difficultyIncrease = 1.01f;
			break;

		case 5:
			spawnRate = 0.9f;
			levelTimeLeft = 60;
			difficultyIncrease = 1.01f;
			break;
		case 6:
			highscoreScreen.SetActive (true);
			highscoreScreen.transform.position = transform.position + posTracker.forward0 * 30 + Vector3.up * 5;
			highscoreScreen.transform.LookAt (transform.position);
			gameOn = false;
			posTracker.calibrationProgress = 5;

			break;
		default:
			Debug.Log ("No level :(");
			break;
		}
		if (currentLevel == 6) {
			return;
		}
		levelMaxTime = levelTimeLeft;
		posTracker.waistIndicatorReference.transform.GetChild (0).gameObject.SetActive (true);
		spawnTimeLeft = spawnRate;
		posTracker.health = 100;
		posTracker.distanceDamage = 0;
		posTracker.angleDamage = 0;
		posTracker.enemyDamage = 0;
		posTracker.UpdateAngles ();
		posTracker.waistIndicColor.color = new Color (0, 1, 0);
		posTracker.waistIndicColor.SetColor ("_EmissionColor", posTracker.waistIndicColor.color);
		PoseTracker.multiplier = (int)Mathf.Round ((posTracker.leftAngleMax / 5 + posTracker.rightAngleMax / 5) * 3 + currentDifficultySpeed * 4);
		posTracker.angles.Add (posTracker.leftAngleMax + posTracker.rightAngleMax);
		score.transform.GetChild (0).GetComponent<TextMeshPro> ().text = "Current multiplier: x" + PoseTracker.multiplier;
		score.visibleScore = 0;
		score.transform.position = transform.position + posTracker.forward0 * 40 + Vector3.up * 5;
		score.transform.LookAt (transform.position);
		score.transform.eulerAngles = new Vector3 (0, score.transform.eulerAngles.y + 180, 0);
		posTracker.sessionTimerText.transform.position = transform.position + posTracker.forward0 * 30 + Vector3.up * 8 + posTracker.right0 * (-30);
		if (currentLevel == 1) {
			score.visibleScore = 0;
			TotalScore.totalScore = 0;
			StartCoroutine (Tutorial ());
		}
	}

	IEnumerator Tutorial ()
	{
		Debug.Log ("Starting tutorial!");
		float tempSpawnRate = spawnRate;
		spawnRate = 999999;
		levelTimeLeft = 99999;
		posTracker.calibrationProgress = 2;
		spawnTimeLeft = spawnRate;
		score.gameObject.SetActive (false);
		gameOn = false;
		posTracker.SetHint ("Rotate your upper body to deflect incoming meteors!");
		yield return new WaitForSeconds (8);
		posTracker.SetHint ("Keep your hips still or you will break the evacuation and lose!");
		yield return new WaitForSeconds (8);
		spawnRate = tempSpawnRate;
		spawnTimeLeft = spawnRate;
		posTracker.SetHint ("Good luck!");
		yield return new WaitForSeconds (3);
		posTracker.calibrationProgress = 3;
		gameOn = true;
		if (posTracker.sessionTimeLeft == posTracker.sessionTime) {
			posTracker.sessionTimeLeft -= Time.deltaTime;
			posTracker.sessionTimerText.gameObject.SetActive (true);
			posTracker.sessionTimerText.transform.position = transform.position + posTracker.forward0 * 40 + Vector3.up * 8 + posTracker.right0 * (-30);
			posTracker.sessionTimerText.transform.LookAt (transform);
			posTracker.sessionTimerText.transform.Rotate (0, 180, 0, Space.World);
		}
		score.gameObject.SetActive (true);
		levelTimeLeft = level1Time;
		posTracker.hintText.gameObject.SetActive (false);
		posTracker.tutManAnim.gameObject.SetActive (false);
	}

	public void SpawnEnemy ()
	{
		Vector3 spawnPosition = posTracker.waistJoint0Pos;
		float randomNum = 0;
		float randomNum2;
		do {
			randomNum = Random.Range (0, 100);
			//   Debug.Log(randomNum);
			randomNum /= 100;
			randomNum2 = Random.Range (0, 100);
			randomNum2 /= 100;
			//Debug.Log("Evaluate " + randomNum + ": " + spawnCurve.Evaluate(randomNum) + ", is it bigger than randomNum2: " + randomNum2 + "?");
		} while (spawnCurve.Evaluate (randomNum) > randomNum2);

		float spawnAngle = randomNum * (posTracker.rightAngleMax - (-posTracker.leftAngleMax)) + (-posTracker.leftAngleMax);

		spawnPosition += Quaternion.AngleAxis (spawnAngle, Vector3.up) * (posTracker.forward0 * 35);
		spawnPosition = new Vector3 (spawnPosition.x, posTracker.chest.transform.position.y, spawnPosition.z);
		allEnemies.Add (Instantiate (enemy, spawnPosition, Quaternion.identity));
		EnemyBehavior tempEnemy = allEnemies [allEnemies.Count - 1].GetComponent<EnemyBehavior> ();
		tempEnemy.player = gameObject;
		tempEnemy.speed *= currentDifficultySpeed;
		PoseTracker.multiplier = (int)Mathf.Round ((posTracker.leftAngleMax / 5 + posTracker.rightAngleMax / 5) * 3 + currentDifficultySpeed * 4);
		score.transform.GetChild (0).GetComponent<TextMeshPro> ().text = "Current multiplier: x" + PoseTracker.multiplier;
		AudioSource.PlayClipAtPoint (spawnSound, spawnPosition);
	}

	public void PauseGame ()
	{
		gameOn = false;
		gamePaused = true;
	}

	public void UnPauseGame ()
	{
		gameOn = true;
		gamePaused = false;
        posTracker.calibrationProgress = 4;
        DialogueManager.source.player.Stop ();
	}

	public void EndGame ()
	{
		Debug.Log ("Ending game..." + "Angledamage: " + posTracker.angleDamage + ", Positiondamage: " + posTracker.distanceDamage + ", hitDamage: " + posTracker.enemyDamage);
		AudioSource.PlayClipAtPoint (gameOverSound, transform.position);
		foreach (GameObject enemy in allEnemies) {
			if (enemy != null)
				enemy.GetComponent<EnemyBehavior> ().Die (enemy.transform.position + Vector3.up);
		}
		enemyPointerLeft.SetActive (false);
		enemyPointerRight.SetActive (false);
		allEnemies.Clear ();
		posTracker.waistIndicatorReference.transform.GetChild (0).gameObject.SetActive (false);

		gameOn = false;
		if (currentLevel < 5) {
			diffScreen.gameObject.SetActive (true);
			diffScreen.transform.position = transform.position + posTracker.forward0 * 40 + Vector3.up * 5;
			diffScreen.transform.LookAt (transform.position);
			currentTotalAngle.text = "Current total angle: " + (int)(posTracker.leftAngleMax + posTracker.rightAngleMax);
		} else {
			if (posTracker.health <= 0) {
				diffScreen.gameObject.SetActive (true);
				diffScreen.transform.position = transform.position + posTracker.forward0 * 40 + Vector3.up * 5;
				diffScreen.transform.LookAt (transform.position);
			}
		}
		posTracker.sessionTimerText.transform.Translate (new Vector3 (-50, 0, 0), Space.Self);
		score.transform.Translate (new Vector3 (50, 0, 0), Space.Self);
		posTracker.calibrationProgress = 5;
		if (posTracker.health > 0)
			score.AddToTotalScore ();
		else {
			score.visibleScore = 0;
			score.text.text = "SCORE: " + score.visibleScore;
		}
	}

	public void NextLevel ()
	{
		currentLevel++;
		if (currentLevel == 6)
			StartGame ();

	}

}
