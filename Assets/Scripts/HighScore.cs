﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;
using System.Linq;
using System;

public class HighScore : MonoBehaviour
{

	public List<Transform> alphabet = new List<Transform> ();
	public Transform back, submit;
	public Transform pointer;
	public TextMeshPro highScore, nameSpace;
	public string[] name = new string[3];
	public GameObject player;
	PoseTracker posTracker;
	Transform currentSelected;
	public List<int> scores = new List<int> ();
	public List<string> names = new List<string> ();
	public List <PlayerScore> playerScores = new List<PlayerScore> ();

	public	struct PlayerScore
	{
		public string name;
		public int score;

		public PlayerScore (string name, int score)
		{
			this.name = name;
			this.score = score;
		}
	};
	// Use this for initialization
	void Start ()
	{
		for (int i = 0; i < 3; i++) {
			name [i] = " _";
		}
		currentSelected = back;
		posTracker = player.GetComponent<PoseTracker> ();
		UpdateScores ();

	}
	
	// Update is called once per frame
	void Update ()
	{
		Ray playerForwardRay = new Ray (player.transform.position, player.transform.forward);
		RaycastHit hit;
		if (Physics.Raycast (playerForwardRay, out hit, (transform.position - player.transform.position).magnitude * 1.5f)) {
			if (alphabet.Contains (hit.transform) || hit.transform == back || hit.transform == submit) {
				if (hit.transform == currentSelected) {

				} else {
					if (currentSelected != null) {
						currentSelected.GetComponent<MeshRenderer> ().material.color = Color.blue;
						currentSelected.GetComponent<MeshRenderer> ().material.SetColor ("_EmissionColor", Color.blue);
						currentSelected.localScale /= 1.1f;
					}
					currentSelected = hit.transform;
					currentSelected.localScale *= 1.1f;
					currentSelected.GetComponent<MeshRenderer> ().material.color = Color.yellow;
					currentSelected.GetComponent<MeshRenderer> ().material.SetColor ("_EmissionColor", Color.yellow);

				}
			} else {
				if (currentSelected != null) {
					currentSelected.GetComponent<MeshRenderer> ().material.color = Color.blue;
					currentSelected.GetComponent<MeshRenderer> ().material.SetColor ("_EmissionColor", Color.blue);
					currentSelected.localScale /= 1.1f;
					currentSelected = null;
				}
			}
            
		} else {
			if (currentSelected != null) {
				currentSelected.GetComponent<MeshRenderer> ().material.color = Color.blue;
				currentSelected.GetComponent<MeshRenderer> ().material.SetColor ("_EmissionColor", Color.blue);
				currentSelected.localScale /= 1.1f;
				currentSelected = null;
			}
		}
		pointer.position = player.transform.position + player.transform.forward * 15;
		pointer.LookAt (player.transform);
        
		if (Input.GetKeyDown ("joystick button 2") || Input.GetKeyDown ("joystick button 0") || Input.GetKeyDown ("joystick button 8") || Input.GetKeyDown ("joystick button 9") || Input.GetKeyDown("joystick button 14") || Input.GetKeyDown("joystick button 15")) { //Either controller's menu button
		
			if (alphabet.Contains (currentSelected)) {
				Debug.Log ("In alphabet!");
				for (int i = 0; i < 3; i++) {
					if (name [i] == " _") {
						name [i] = currentSelected.gameObject.name;
						string currentName = "";
						for (int y = 0; y < 3; y++) {
							currentName += " " + name [y];
						}
						nameSpace.text = "NAME:" + currentName; 
						return;
					}
				}
			} else if (currentSelected == back) {
				Debug.Log ("Delete letter!");
				for (int i = 2; i > -1; i--) {
					if (name [i] != " _") {
						name [i] = " _";
						string currentName = "";
						for (int y = 0; y < 3; y++) {
							currentName += " " + name [y];
						}
						nameSpace.text = "NAME:" + currentName; 
						return;
					}
				}
			} else if (currentSelected == submit) {
				Debug.Log ("submit!");
				string currentName = "";
				for (int y = 0; y < 3; y++) {
					currentName += " " + name [y];
				}
				if (currentName != "_ _ _") {
					File.AppendAllText (Application.dataPath + "/scores/scores.txt", "" + TotalScore.totalScore + "\n");
					File.AppendAllText (Application.dataPath + "/scores/scores.txt", currentName + "\n");
					File.AppendAllText (Application.dataPath + "/scores/angles.txt", currentName + ":\n");
					foreach (float angle in posTracker.angles)
						File.AppendAllText (Application.dataPath + "/scores/angles.txt", angle.ToString("00.00") + "\n");
					UpdateScores ();
					StartCoroutine (Disappear ());
				}
                EnemySpawner.currentLevel = 1;
                player.GetComponent<PoseTracker>().health = 100;
                player.GetComponent<EnemySpawner>().levelTimeLeft = 999;
				player.GetComponent<PoseTracker> ().calibrationProgress = -1;
			}

		}
	}

	public void UpdateScores ()
	{
		names.Clear ();
		scores.Clear ();
		playerScores.Clear ();
		StreamReader file = new StreamReader (Application.dataPath + "/scores/scores.txt");
		string line = file.ReadLine ();
		int counter = 0;
		while ((line = file.ReadLine ()) != null) {

			//Debug.Log (line);

			scores.Add (Int32.Parse (line));
			line = file.ReadLine ();
			names.Add (line);
			playerScores.Add (new PlayerScore (names [names.Count - 1], scores [scores.Count - 1]));
			counter++;
		}
		file.Close ();
		List<PlayerScore> sortedScores = playerScores.OrderByDescending (o => o.score).ToList ();
		highScore.text = "";
		for (int i = 0; i < 11; i++) {
			highScore.text += (i + 1) + "." + sortedScores [i].name + ": " + sortedScores [i].score + "\n";
		}
		submit.GetComponent<BoxCollider> ().enabled = true;
	}

	public IEnumerator Disappear ()
	{
		submit.GetComponent<BoxCollider> ().enabled = false;
		player.GetComponent<PoseTracker> ().calibrationProgress = -1;
		player.GetComponent<PoseTracker> ().startText.gameObject.SetActive (true);
		player.GetComponent<PoseTracker> ().sessionTimeLeft = player.GetComponent<PoseTracker> ().sessionTime;
		foreach (Transform letter in alphabet) {
			letter.gameObject.SetActive (false);
		}
		back.gameObject.GetComponent<BoxCollider> ().enabled = false;
		submit.gameObject.GetComponent<BoxCollider> ().enabled = false;
		player.GetComponent<PoseTracker> ().ResetScene ();
		yield return new WaitForSeconds (10);
		foreach (Transform letter in alphabet) {
			letter.gameObject.SetActive (true);
		}
		back.gameObject.GetComponent<BoxCollider> ().enabled = true;
		submit.gameObject.GetComponent<BoxCollider> ().enabled = true;
		gameObject.SetActive (false);

	}

}
