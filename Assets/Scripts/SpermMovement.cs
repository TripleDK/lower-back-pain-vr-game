﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpermMovement : MonoBehaviour
{

	ParticleSystem particles;
	public Transform start, mid, end;
	public List<GameObject> gParticles;
	float timer;
	public bool broken;

	// Use this for initialization
	void Start ()
	{
		particles = GetComponent<ParticleSystem> ();
		timer = 0;
		for (int i = 0; i < transform.childCount; i++) {
			gParticles.Add (transform.GetChild (i).gameObject);
		}
		broken = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		timer += Time.deltaTime;
		for (int i = 0; i < gParticles.Count; i++) {
			float tempTimer = timer + i / (100f / gParticles.Count);
			tempTimer = tempTimer % 1;
			//Debug.Log ("i: " + i + ", tempTimer = " + tempTimer + ", extra: " + i / (100f / gParticles.Count));
			if (!broken) {
				gParticles [i].transform.position = (1 - tempTimer) * (1 - tempTimer) * start.position + 2 * (1 - tempTimer) * tempTimer * mid.position + tempTimer * tempTimer * end.position;
			} else {
				gParticles [i].transform.position = (1 - tempTimer) * start.position + tempTimer * (mid.position + (mid.position - start.position) * 1.5f);
			}
		}
        
	}
}
