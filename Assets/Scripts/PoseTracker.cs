﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class PoseTracker : MonoBehaviour
{

	public float currentRotation;
	public Transform head, chest, waist, chestController, waistController, headJoint, chestJoint, waistJoint, middleBodyJoint, angleWall1, angleWall2;
	Vector3 waistStartPos, chestStartPos, chestJointStartPos;
	int waistControllerIndex, chestControllerIndex;
	public RobotChild robotRoot;
	public Vector3 headJointOffset, chestJointOffset, waistJointOffset;
	Animator waistAnim, shieldAnim;
	public Animator robotAnim, tutManAnim, ringAnim;
	public GameObject waistIndicator, waistIndicatorReference, shield, shieldPointer, waistChecker, startArrow1, startArrow2;
	public TextMeshPro startText, hintText, hintText2, score;
	public Material waistIndicColor, ringColor, buttonHighlights, waistCheckerColor;
	public int calibrationProgress = -1;
	public GameObject gazeButton;
	public Vector3 head0Pos, chest0Pos, waist0Pos, waistJoint0Pos, shield0Pos, right0, forward0 = Vector3.zero;
	public float leftAngleMax, rightAngleMax, headToChest;
	public float waistThreshold = 5;
	public float waistTresholdDistance;
	public float slouchLimit = 3;
	public float health = 100;
	public static int multiplier = 1;
	public AudioClip confirmationSound, gameStartSound, errorSound, loseSound, timeUpSound;
	public AudioSource engineSound;
	Color backgroundCol, groundCol;
	ParticleSystem spermParticles;
	public GameObject spermParticles2;
	SpermMovement spermMov;
	public Material groundMat;
	public GameObject spaceBackground, earth;
	EnemySpawner enemySpawn;
	Camera mainCam;
	Skybox skyBox;
	public float distanceDamage, angleDamage, enemyDamage;
	EnemySpawner enSpa;
	ParticleSystem ringParticles;
	public float sessionTimeLeft;
	public float sessionTime;
	public TextMeshPro sessionTimerText;
	SteamVR_TrackedObject leftSteamController, rightSteamController;
	public  List<float> angles = new List<float> ();
	bool controllersFound = false;

	// Use this for initialization
	void Start ()
	{
		currentRotation = 0;
		waistAnim = waistIndicator.GetComponent<Animator> ();
		shieldAnim = shield.GetComponent<Animator> ();
		ringParticles = ringAnim.transform.GetChild (0).GetComponent<ParticleSystem> ();

		waistStartPos = waist.transform.localPosition;
		chestStartPos = chest.transform.localPosition;
		chestJointStartPos = chestJoint.transform.localPosition;

		enemySpawn = GetComponent<EnemySpawner> ();
		mainCam = GetComponent<Camera> ();
		leftSteamController =	waistController.gameObject.GetComponent<SteamVR_TrackedObject> ();
		rightSteamController = chestController.gameObject.GetComponent<SteamVR_TrackedObject> ();
		spermMov = spermParticles2.GetComponent<SpermMovement> ();
		skyBox = mainCam.GetComponent<Skybox> ();
		enSpa = GetComponent<EnemySpawner> ();
		spermParticles = waistIndicatorReference.transform.GetChild (0).gameObject.GetComponent<ParticleSystem> ();
		ResetScene ();
		if (!File.Exists (Application.dataPath + "/scores/angles.txt")) {
			Debug.Log ("Creating angle file");
			File.WriteAllText (Application.dataPath + "/scores/angles.txt", "Angles: \n\n");
		}
		StartCoroutine (DelayedStart ());
	}

	IEnumerator DelayedStart ()
	{
		bool done = false;
        while (!done)
        {
            Debug.Log("Checking for controllers");
            done = true;
            yield return new WaitForSeconds(1);
            if (waistController != null && waistController.gameObject.activeSelf)
                //	waistController.FindChild ("Model").FindChild ("button").GetComponent<MeshRenderer> ().material = buttonHighlights;
                waistController.FindChild("Model").FindChild("trigger").GetComponent<MeshRenderer>().material = buttonHighlights;
            else
                done = false;
            if (chestController != null && chestController.gameObject.activeSelf)
                //	chestController.FindChild ("Model").FindChild ("button").GetComponent<MeshRenderer> ().material = buttonHighlights;
                chestController.FindChild("Model").FindChild("trigger").GetComponent<MeshRenderer>().material = buttonHighlights;
            else
            {
                done = false;
                startText.text = "Controllers missing";
            }
        }
		Debug.Log ("Done finding controllers!");
        startText.text = "Press the trigger button to start!";
		controllersFound = true;
	}

	// Update is called once per frame
	void Update ()
	{
		//  Debug.Log("Axis input detected: " + Input.GetAxis("joystick axis 1") + ", " + Input.GetAxis("joystick axis 2"));
		/*	if (Input.GetKeyDown (KeyCode.C)) {
			CalibrateStraight ();
		}
		if (Input.GetKeyDown (KeyCode.X)) {
			CalibrateLeft ();
		}
		if (Input.GetKeyDown (KeyCode.V)) {
			CalibrateRight ();
		}*/
		int numOfControllersActive = 0;
		if (waistController.gameObject.activeSelf)
			numOfControllersActive++;
		if (chestController.gameObject.activeSelf)
			numOfControllersActive++;
		if (numOfControllersActive < 2) {
			if (EnemySpawner.gameOn && EnemySpawner.gamePaused == false) {
				enSpa.PauseGame ();
				SetWarning ("A controller is not connected!");
			}
		} else {
			if (startText.text == "A controller is not connected!") {
				SetWarning ("");
			}
		}

		if (calibrationProgress >= 0 && sessionTimeLeft != sessionTime) {
			sessionTimeLeft -= Time.deltaTime;
			sessionTimerText.text = "Time left: " + ((int)(sessionTimeLeft / 60)).ToString ("0") + ":" + ((int)sessionTimeLeft % 60).ToString ("00");
			if (sessionTimeLeft <= 0) {
				sessionTimeLeft = sessionTime;
				sessionTimerText.text = "00.00";
				AudioSource.PlayClipAtPoint (timeUpSound, transform.position);
				EnemySpawner.currentLevel = 5;
				enSpa.EndGame ();
				enSpa.NextLevel ();
				//calibrationProgress = -1;
				enSpa.diffScreen.gameObject.SetActive (false);
			
			}
		}

		if (forward0 != Vector3.zero) {
			CheckWaist ();
		}												//14 & 15: triggers
		if (numOfControllersActive == 2 && (Input.GetKeyDown ("joystick button 14") || Input.GetKeyDown ("joystick button 15")) /*Input.GetKeyDown ("joystick button 2") || Input.GetKeyDown ("joystick button 0") *//*|| Input.GetKeyDown ("joystick button 8") || Input.GetKeyDown ("joystick button 9")*/) { //Either controller's menu button
			Debug.Log ("Pressed a trigger button");
			if (calibrationProgress < 1 || calibrationProgress > 2) // leave those to Gaze Buttons
				StartCoroutine (Calibrate ());
			AudioSource.PlayClipAtPoint (confirmationSound, transform.position);
		}
		if (calibrationProgress > 0) {
			headJoint.position = head.transform.position + headJointOffset;
			waistJoint.position = waist.transform.position + waistJointOffset;
			chestJoint.position = chest.transform.position + chestJointOffset;

			waistJoint.eulerAngles = new Vector3 (waistJoint.eulerAngles.x, waist.eulerAngles.y, waistJoint.eulerAngles.z);
			chestJoint.eulerAngles = new Vector3 (chestJoint.eulerAngles.x, chest.eulerAngles.y, chestJoint.eulerAngles.z);
			Debug.DrawLine (chestJoint.position, chestJoint.position + chest.transform.forward * 30, Color.red);
		}
	}

	public IEnumerator Calibrate ()
	{

		//yield return new WaitForSeconds (DialogueManager.source.player.clip.length);
		switch (calibrationProgress) {
		case -1:
		//	if (EnemySpawner.currentLevel != 6) {
			calibrationProgress = 0;
			DialogueManager.source.player.clip = DialogueManager.source.calibrationDialogue [0];
			DialogueManager.source.player.Play ();
			
			startText.gameObject.SetActive (false);
			enSpa.highscoreScreen.gameObject.SetActive (false);
				//    startText.text = "";
				//  Destroy(startText.gameObject);
			SetHint ("Straighten your back and then press the trigger button!");
			
		
		/*	} else {
				EnemySpawner.currentLevel = 1;
				enSpa.StartGame ();
				waistIndicColor.color = new Color (1 - health / 100, health / 100, 0);
				waistIndicColor.SetColor ("_EmissionColor", waistIndicColor.color);
			}*/
			break;
		case 0:
			CalibrateStraight ();
			tutManAnim.SetTrigger ("StretchLeft");
			SetHint ("Keep your hips still, and rotate your upper body as much as you can to the LEFT!");
			calibrationProgress++;
                //	SetHint2 ("When rotated as much as possible, look at the blue square to continue!");
			SetHint2 ("");
			if (DialogueManager.source.player.isPlaying)
				DialogueManager.source.player.Stop ();
			DialogueManager.source.player.clip = DialogueManager.source.calibrationDialogue [1];
			DialogueManager.source.player.Play ();
			yield return new WaitForSeconds (DialogueManager.source.player.clip.length * (1 + (1 - DialogueManager.source.player.pitch))+0.3f);
                Debug.Log("Tell you to turn left!");
                if (!DialogueManager.source.player.isPlaying && calibrationProgress <= 1) {
				DialogueManager.source.player.clip = DialogueManager.source.calibrationDialogue [2];
				DialogueManager.source.player.Play ();
			}
			break;
		case 1:
			if (CheckWaist ()) {
				StartCoroutine (CalibrateLeft ());
				SetHint ("Keep your hips still, and rotate your upper body as much as you can to the RIGHT!");
				hintText.transform.position = transform.position + forward0 * 30 - transform.up * 0;
				hintText.transform.LookAt (transform);
				hintText.transform.Rotate (new Vector3 (0, 180, 0));
				tutManAnim.transform.position = transform.position + forward0 * 30 + transform.up * 5;
				tutManAnim.transform.LookAt (transform);
				tutManAnim.transform.Rotate (new Vector3 (0, 180, 0));
				tutManAnim.SetTrigger ("StretchRight");
				
				if (DialogueManager.source.player.isPlaying)
					DialogueManager.source.player.Stop ();
				DialogueManager.source.player.clip = DialogueManager.source.calibrationDialogue [3];
				DialogueManager.source.player.Play ();
				calibrationProgress++;
				//	SetHint2 ("When rotated as much as possible, look at the blue square to continue!");
				SetHint2 ("");
			} else {
				AudioSource.PlayClipAtPoint (errorSound, transform.position);
				StartCoroutine (Vibrate (0.5f));
				SetHint2 ("Remember to keep your hips pointing forward! Then press the trigger button to continue!");
				DialogueManager.source.PlayErrorStart ();
			
			}
			break;
		case 2:
			if (CheckWaist ()) {
				StartCoroutine (CalibrateRight ());
				AudioSource.PlayClipAtPoint (gameStartSound, transform.position);
				waistIndicColor.color = new Color (0, 1, 0, 0.5f);
				startArrow1.SetActive (false);
				startArrow2.SetActive (false);
				tutManAnim.gameObject.SetActive (false);
				hintText.gameObject.SetActive (false);
				hintText2.gameObject.SetActive (false);
				shieldPointer.SetActive (false);
				gazeButton.SetActive (false);

				score.gameObject.SetActive (true);
				score.transform.position = transform.position + forward0 * 40 + Vector3.up * 5;
				score.transform.LookAt (transform.position);
				score.transform.eulerAngles = new Vector3 (0, score.transform.eulerAngles.y + 180, 0);
				if (EnemySpawner.gamePaused == false) {
					if (DialogueManager.source.player.isPlaying)
						DialogueManager.source.player.Stop ();
					DialogueManager.source.player.clip = DialogueManager.source.calibrationDialogue [4];
					DialogueManager.source.player.Play ();
                    
					enSpa.StartGame ();
					Debug.Log ("Should start game");
					waistIndicColor.color = new Color (1 - health / 100, health / 100, 0);
					waistIndicColor.SetColor ("_EmissionColor", waistIndicColor.color);
				} else {
					Debug.Log ("Resuming game!");
					enSpa.UnPauseGame ();
				}
				//calibrationProgress = 3;
			} else {
				AudioSource.PlayClipAtPoint (errorSound, transform.position);
				StartCoroutine (Vibrate (0.5f));
				DialogueManager.source.PlayErrorStart ();
				SetHint2 ("Remember to keep your hips pointing forward! Then press the trigger button to continue!");
			}
			break;
		case 3:
		case 4:
			calibrationProgress = 0;
			TurnRingOff ();
			robotRoot.enabled = true;
			startArrow1.SetActive (true);
			startArrow2.SetActive (true);
			tutManAnim.gameObject.SetActive (true);
			angleWall1.gameObject.SetActive (false);
			angleWall2.gameObject.SetActive (false);
			score.gameObject.SetActive (false);
			shieldPointer.SetActive (true);
			waist.SetParent (robotRoot.transform);
			chest.SetParent (robotRoot.transform);
			waistJoint.transform.localPosition = Vector3.zero;
			chestJoint.transform.localPosition = chestJointStartPos;
			waist.transform.localPosition = waistStartPos;
			chest.transform.localPosition = chestStartPos;
			waist.transform.localEulerAngles = new Vector3 (0, -90, 0);
			chest.transform.localEulerAngles = new Vector3 (0, -90, 0);
			headJointOffset = Vector3.zero;
			waistJointOffset = Vector3.zero;
			chestJointOffset = Vector3.zero;
			tutManAnim.SetTrigger ("StopStretch");
			SetHint ("Straigthen your back and then press the trigger button!");
                DialogueManager.source.player.clip = DialogueManager.source.calibrationDialogue[0];
                DialogueManager.source.player.Play();
                enSpa.PauseGame ();
			break;
		case 5:
			break;
		case 6:
			break;
		default:
			Debug.Log ("Calibration progress is broken!");
			break;
		}
		yield return new WaitForSeconds (0);
	}

	public void CalibrateStraight ()
	{
		robotRoot.enabled = false;
		waistChecker.SetActive (true);
		waistIndicColor.color = new Color (1 - health / 100, health / 100, 0);
		waistIndicColor.SetColor ("_EmissionColor", waistIndicColor.color);
		if (chestController.transform.position.y < waistController.transform.position.y) {
			Transform tempChest = chestController;
			chestController = waistController;
			waistController = tempChest;
		}
		shield.SetActive (true);
		shieldPointer.SetActive (true);
		waistAnim.gameObject.SetActive (true);
		spermParticles2.SetActive (true);
		waist.SetParent (waistController);
		chest.SetParent (chestController);
		waistControllerIndex = (int)waistController.gameObject.GetComponent<SteamVR_TrackedObject> ().index;
		chestControllerIndex = (int)chestController.gameObject.GetComponent<SteamVR_TrackedObject> ().index;

		//chest.transform.localPosition = Vector3.zero;
		//	waist.transform.localPosition = Vector3.zero;
		//      chest.transform.localEulerAngles = tempChestRot;
		//    waist.transform.localEulerAngles = tempWaistRot;

		headJointOffset = headJoint.position - head.position;
		chestJointOffset = chestJoint.position - chest.position;
		waistJointOffset = waistJoint.position - waist.position;

		head0Pos = head.position;
		chest0Pos = chestController.position;
		shield0Pos = chest.position;
		waist0Pos = waistController.position;
		waistJoint0Pos = waistJoint.position;
		forward0 = waist.forward;
		right0 = waist.right;


		waistChecker.transform.position = waist.transform.position + forward0 * 12;
		waistChecker.transform.LookAt (waist.transform);
		StartCoroutine (SendSperm ());
		headToChest = head.transform.position.y - chest.transform.position.y;
		spaceBackground.SetActive (true);
		earth.transform.position = waist0Pos - forward0 * 300;
		waistAnim.SetTrigger ("Extend");
		robotAnim.SetTrigger ("Extend");
		shieldAnim.SetTrigger ("Expand");
		TurnRingOn ();
	}

	public void UpdateAngles ()
	{

		angleWall1.gameObject.SetActive (true);

		angleWall1.eulerAngles = new Vector3 (0, waist.eulerAngles.y + 90, 0);

		angleWall1.position = waistJoint0Pos;

		angleWall1.Translate (new Vector3 (-angleWall1.GetComponent<MeshRenderer> ().bounds.extents.x, angleWall1.GetComponent<MeshRenderer> ().bounds.extents.y, 0), Space.Self);

		angleWall1.transform.RotateAround (waistJoint0Pos, Vector3.up, -leftAngleMax);

		angleWall1.Translate (new Vector3 (-20, 0, 0), Space.Self);

		angleWall2.gameObject.SetActive (true);

		angleWall2.eulerAngles = new Vector3 (0, waist.eulerAngles.y + 90, 0);

		angleWall2.position = waistJoint0Pos;

		angleWall2.Translate (new Vector3 (-angleWall2.GetComponent<MeshRenderer> ().bounds.extents.x, angleWall2.GetComponent<MeshRenderer> ().bounds.extents.y, 0), Space.Self);

		angleWall2.transform.RotateAround (waistJoint0Pos, Vector3.up, rightAngleMax);

		angleWall2.Translate (new Vector3 (-20, 0, 0), Space.Self);
	}

	IEnumerator CalibrateLeft ()
	{
		leftAngleMax = Vector3.Angle (forward0, chest.forward);
		angleWall1.gameObject.SetActive (true);
		//      yield return new WaitForSeconds(1);
		angleWall1.eulerAngles = new Vector3 (0, waist.eulerAngles.y + 90, 0);
		//     yield return new WaitForSeconds(1);
		angleWall1.position = waistJoint0Pos;
		//      yield return new WaitForSeconds(1);
		angleWall1.Translate (new Vector3 (-angleWall1.GetComponent<MeshRenderer> ().bounds.extents.x, angleWall1.GetComponent<MeshRenderer> ().bounds.extents.y, 0), Space.Self);
		//     yield return new WaitForSeconds(1);
		angleWall1.transform.RotateAround (waistJoint0Pos, Vector3.up, -leftAngleMax);
		//   yield return new WaitForSeconds(1);
		angleWall1.Translate (new Vector3 (-20, 0, 0), Space.Self);
		yield return null;
	}

	IEnumerator CalibrateRight ()
	{
		rightAngleMax = Vector3.Angle (forward0, chest.forward);
		angleWall2.gameObject.SetActive (true);
		//   yield return new WaitForSeconds(1);
		angleWall2.eulerAngles = new Vector3 (0, waist.eulerAngles.y + 90, 0);// angleWall2.rotation = Quaternion.identity
		//    yield return new WaitForSeconds(1);
		angleWall2.position = waistJoint0Pos;
		//    yield return new WaitForSeconds(1);
		angleWall2.Translate (new Vector3 (-angleWall2.GetComponent<MeshRenderer> ().bounds.extents.x, angleWall2.GetComponent<MeshRenderer> ().bounds.extents.y, 0), Space.Self);
		//    yield return new WaitForSeconds(1);
		angleWall2.transform.RotateAround (waistJoint0Pos, Vector3.up, rightAngleMax);
		//   yield return new WaitForSeconds(1);
		angleWall2.Translate (new Vector3 (-20, 0, 0), Space.Self);
		yield return null;
	}


	bool CheckWaist ()
	{
		float distance = 0;
		float angle = Vector3.Angle (waist.forward, forward0);
		Debug.DrawLine (waist0Pos, waist0Pos + forward0 * 50, Color.white);
		Debug.DrawLine (waist0Pos, waist0Pos + waist.forward * 50, Color.green);
		currentRotation = angle;
		if (calibrationProgress == 3) {

			Debug.DrawLine (waist0Pos, waist0Pos + Quaternion.AngleAxis (leftAngleMax, Vector3.up) * forward0 * 5, Color.blue);
			Debug.DrawLine (waist0Pos, waist0Pos + Quaternion.AngleAxis (rightAngleMax, Vector3.up) * forward0 * 5, Color.blue);
			// Debug.DrawLine(transform.position, transform.position + waist.forward * 10, Color.red);
			//  Debug.DrawLine(waist0Pos, waist0Pos + forward0 * 10, Color.yellow);
			distance = (waistController.position - waist0Pos).magnitude;

			backgroundCol = new Color (Mathf.Sin (Mathf.Deg2Rad * angle), Mathf.Cos (Mathf.Deg2Rad * angle), 0);
			ringColor.color = new Color (distance / waistTresholdDistance, 1 - (distance / waistTresholdDistance), 0);
			ringColor.SetColor ("_EmissionColor", ringColor.color);

			//  Debug.Log("R: " + distance / waistTresholdDistance + ", G " + (1 - (distance / waistTresholdDistance)) + "Distance: "+ distance);
			var main = ringParticles.main;
			main.startColor = ringColor.color;
			if (waistIndicatorReference.activeSelf) {
				main = spermParticles.main;
				main.startColor = backgroundCol;
			}
			skyBox.material.SetColor ("_Tint", backgroundCol);
			middleBodyJoint.position = (chest.position + waist.position) / 2;
			engineSound.volume = Mathf.Sin (Mathf.Deg2Rad * angle);

			if (distance > waistTresholdDistance) {

				if (EnemySpawner.gameOn) {
                    distanceDamage += (distance - waistTresholdDistance) * Time.deltaTime;
                    StartCoroutine (ApplyDamage ((distance - waistTresholdDistance) * Time.deltaTime));
					
				}
			}
		}
		if (head0Pos.y - head.position.y > slouchLimit) {
			SetWarning ("You are slouching! Straighten your back!");
		} else {
			if (startText.text == "You are slouching! Straighten your back!") {
				SetWarning ("");
			}
		}
		if (angle > waistThreshold) {
			//	waistIndicColor.color = new Color (1, 0, 0, 0.5f);
			spermMov.broken = true;
			if (EnemySpawner.currentLevel < 6 && calibrationProgress > 0 && calibrationProgress != 5)
				SteamVR_Controller.Input ((int)waistControllerIndex).TriggerHapticPulse ((ushort)800);
			if (EnemySpawner.gameOn) {
				DialogueManager.source.PlayMovedHips ();
                angleDamage += angle * Time.deltaTime;
                StartCoroutine (ApplyDamage (angle * Time.deltaTime));
				

			}
			waistCheckerColor.SetColor ("_EmissionColor", Color.red);
			return false;
		} else {
			spermMov.broken = false;
			//	waistIndicColor.color = new Color (0, 1, 0, 0.5f);
			waistCheckerColor.SetColor ("_EmissionColor", Color.blue);
			return true;
		}

		

	}

	public IEnumerator ApplyDamage (float damage)
	{
		health -= damage;
		waistIndicColor.color = new Color (1 - health / 100, health / 100, 0);
		waistIndicColor.SetColor ("_EmissionColor", waistIndicColor.color);
		StartCoroutine (Vibrate (0.1f));
		if (health <= 20) {
			DialogueManager.source.PlayAlmostLose ();
				
		}

		if (health <= 0) {
			calibrationProgress = 4;
            enSpa.gameInfo.text = "Stability: " + (int)health + "\nProgress: " + Mathf.Max((int)(100 * (1 - enSpa.levelTimeLeft / enSpa.levelMaxTime)), 0) + "%";
            TurnRingOff ();
			angleWall1.gameObject.SetActive (false);
			angleWall2.gameObject.SetActive (false);
			robotRoot.enabled = true;
			AudioSource.PlayClipAtPoint (loseSound, transform.position);
			
			DialogueManager.source.PlayLose ();
				
			enemySpawn.EndGame ();
		}
		yield return new WaitForSeconds (0);
	}

	public void SetHint (string text)
	{
		hintText.gameObject.SetActive (true);
		hintText.text = text;
		hintText.transform.position = transform.position + forward0 * 30;
		if (forward0 == Vector3.zero) {
			hintText.transform.position = transform.position + waist.transform.forward * 30;
			hintText.transform.position = new Vector3 (hintText.transform.position.x, transform.position.y, hintText.transform.position.z);
		}
		hintText.transform.LookAt (transform);
		hintText.transform.Rotate (new Vector3 (0, 180, 0));
		tutManAnim.gameObject.SetActive (true);
		tutManAnim.transform.position = transform.position + forward0 * 30 + Vector3.up * 5;
		if (forward0 == Vector3.zero) {
			tutManAnim.transform.position = transform.position + waist.transform.forward * 30;
			tutManAnim.transform.position = new Vector3 (tutManAnim.transform.position.x, transform.position.y + 5, tutManAnim.transform.position.z);
		}
		tutManAnim.transform.LookAt (transform);
		tutManAnim.transform.Rotate (new Vector3 (0, 180, 0));

	}

	public void SetHint2 (string text)
	{
		hintText2.gameObject.SetActive (true);
		hintText2.text = text;
		if (calibrationProgress == 1) {
			hintText2.transform.position = transform.position - right0 * 25 + forward0 * 5 - Vector3.up * 10;
			gazeButton.SetActive (true);
			gazeButton.transform.position = hintText2.transform.position + Vector3.up * 15;
			gazeButton.transform.LookAt (transform);
			gazeButton.transform.Rotate (0, 90, 90);
		}
		if (calibrationProgress == 2) {
			hintText2.transform.position = transform.position + right0 * 25 + forward0 * 5 - Vector3.up * 10;
			gazeButton.SetActive (true);
			gazeButton.transform.position = hintText2.transform.position + Vector3.up * 15;
			gazeButton.transform.LookAt (transform);
			gazeButton.transform.Rotate (0, 90, 90);
		}
		hintText2.transform.LookAt (transform);
		hintText2.transform.Rotate (new Vector3 (0, 180, 0));
	}


	public void SetWarning (string text)
	{
		startText.gameObject.SetActive (true);
		startText.text = text;
	}

	void TurnRingOn ()
	{

		ringAnim.speed = 1;
		ringColor.color = Color.green;
		ringColor.SetColor ("_EmissionColor", Color.green);
		ringAnim.gameObject.transform.GetChild (0).gameObject.SetActive (true);
	}

	void TurnRingOff ()
	{
		ringAnim.speed = 0;
		ringColor.color = Color.black;
		ringColor.SetColor ("_EmissionColor", Color.black);
		ringAnim.gameObject.transform.GetChild (0).gameObject.SetActive (false);
	}

	IEnumerator Vibrate (float seconds)
	{
		float timer = 0;
		while (timer < seconds) {
			SteamVR_Controller.Input ((int)chestControllerIndex).TriggerHapticPulse ((ushort)800);
			//  Debug.Log("Vibrate the chest dammit!");
			yield return new WaitForEndOfFrame ();
			timer += Time.deltaTime;
		}
	}

	IEnumerator SendSperm ()
	{
		yield return new WaitForSeconds (0);

		waistIndicatorReference.SetActive (true);
		//Debug.Log (waistIndicator.transform.position);
		waistIndicatorReference.transform.position = waistIndicator.transform.position;
		waistIndicatorReference.transform.rotation = waistIndicator.transform.rotation;

	}

	public void ResetScene ()
	{
		TurnRingOff ();
		skyBox.material.SetColor ("_Tint", new Color (0.5f, 0.5f, 0.5f));
		spermParticles2.SetActive (false);
		hintText.gameObject.SetActive (false);
		hintText2.gameObject.SetActive (false);
		shield.SetActive (false);
		shieldPointer.SetActive (false);
		waistAnim.gameObject.SetActive (false);
		tutManAnim.gameObject.SetActive (false);
		angleWall1.gameObject.SetActive (false);
		angleWall2.gameObject.SetActive (false);
		waistIndicatorReference.SetActive (false);
		gazeButton.SetActive (false);
		sessionTimerText.gameObject.SetActive (false);
		sessionTimeLeft = sessionTime;
		score.gameObject.SetActive (false);
		sessionTimerText.gameObject.SetActive (false);
		spaceBackground.SetActive (false);
		skyBox.material.SetColor ("_Tint", Color.black);
		waistIndicColor.color = new Color (0, 1, 0);
		waistIndicColor.SetColor ("_EmissionColor", waistIndicColor.color);
		waistChecker.SetActive (false);
		waistTresholdDistance = ringAnim.GetComponent<MeshRenderer> ().bounds.extents.x;
	}
}