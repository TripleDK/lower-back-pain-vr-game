﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingSaucer : MonoBehaviour {

	public float speed = 0.2f;
	public float rotateSpeed = 100;
	public float diameter = 15;
    public GameObject meteor;
    public float meteorSpawner = 4;
    float spawnerLeft;
    public float meteorVariability = 2;
	Vector3 startPos;
    float randomStart;

	// Use this for initialization
	void Start () {
		startPos = transform.position;
        randomStart = Random.Range(0, 2 * Mathf.PI);
        spawnerLeft = meteorSpawner + Random.Range(-meteorVariability, meteorVariability);
    }
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(startPos.x+diameter*Mathf.Cos(Time.time*speed+randomStart), transform.position.y, startPos.z+diameter*Mathf.Sin(Time.time*speed+randomStart));
		transform.Rotate (0, rotateSpeed*Time.deltaTime, 0);
        spawnerLeft -= Time.deltaTime;
        if(spawnerLeft <= 0)
        {
            Instantiate(meteor, transform.position, Quaternion.identity);
            spawnerLeft = meteorSpawner + Random.Range(-meteorVariability, meteorVariability);
        }
	}
}
