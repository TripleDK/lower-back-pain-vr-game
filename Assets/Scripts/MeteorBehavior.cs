﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorBehavior : MonoBehaviour
{

	public GameObject deathParticles;
	public Transform earth;
	MeshFilter mesh;
	public List<Mesh> meteorMesh = new List<Mesh> ();
	public float speed = 10;

	// Use this for initialization
	void Start ()
	{
		mesh = GetComponent<MeshFilter> ();
		mesh.mesh = meteorMesh [Random.Range (0, meteorMesh.Count)];
		earth = GameObject.Find ("Earth").transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.position = Vector3.MoveTowards (transform.position, earth.position, speed * Time.deltaTime);
	}

	void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.name == "Earth") {
			GameObject tempParticles = (GameObject)Instantiate (deathParticles, transform.position, Quaternion.identity) as GameObject;
			tempParticles.transform.localScale *= 6;
			Destroy (tempParticles, 3);
			Destroy (gameObject);
		}
	}

}
