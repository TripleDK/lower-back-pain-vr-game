﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueManager : MonoBehaviour
{

	public List<AudioClip> calibrationDialogue = new List<AudioClip> ();
	public List<AudioClip> errorStart = new List<AudioClip> ();
	public List<AudioClip> movedHips = new List<AudioClip> ();
	public List<AudioClip> lose = new List<AudioClip> ();
	public List<AudioClip> win = new List<AudioClip> ();
	public List<AudioClip> almostWin = new List<AudioClip> ();
	public List<AudioClip> almostLose = new List<AudioClip> ();
  
	public static DialogueManager source;
	public AudioSource player;

	int prevErrorStart, prevMovedHips, prevLose, prevWin, prevAlmostWin, prevAlmostLose;
	int clipCounter;
	public float hipErrorTimer = 0;

	// Use this for initialization
	void Start ()
	{
		if (source == null)
			source = this;
		else
			Debug.Log ("You might have two dialogue managers!");
		player = GetComponent<AudioSource> ();
		prevErrorStart = -1;
		prevMovedHips = -1;
		prevLose = -1;
		prevWin = -1;
		prevAlmostWin = -1;
		prevAlmostLose = -1;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (hipErrorTimer > 0)
			hipErrorTimer -= Time.deltaTime;
	}

	public void PlayErrorStart ()
	{
		if (DialogueManager.source.player.isPlaying)
			DialogueManager.source.player.Stop ();
		do {
			clipCounter = Random.Range (0, DialogueManager.source.errorStart.Count);
		} while(clipCounter == prevErrorStart);
		prevErrorStart = clipCounter;
		DialogueManager.source.player.clip = DialogueManager.source.errorStart [clipCounter];
		DialogueManager.source.player.Play ();

	}

	public void PlayMovedHips ()
	{
		if (!DialogueManager.source.player.isPlaying && hipErrorTimer <= 0) {	
			do {
				clipCounter = Random.Range (0, DialogueManager.source.movedHips.Count);
			} while(clipCounter == prevMovedHips);
			prevMovedHips = clipCounter;
			DialogueManager.source.player.clip = DialogueManager.source.movedHips [clipCounter];
			DialogueManager.source.player.Play ();
			hipErrorTimer = 5;
		}
	}

	public void PlayAlmostLose ()
	{
		if (!DialogueManager.source.player.isPlaying) {	
			do {
				clipCounter = Random.Range (0, DialogueManager.source.almostLose.Count);
			} while(clipCounter == prevAlmostLose);
			prevAlmostLose = clipCounter;
			DialogueManager.source.player.clip = DialogueManager.source.almostLose [clipCounter];
			DialogueManager.source.player.Play ();
		}
	}

	public void PlayAlmostWin ()
	{
		if (!DialogueManager.source.player.isPlaying) {	
			do {
				clipCounter = Random.Range (0, DialogueManager.source.almostWin.Count);
			} while(clipCounter == prevAlmostWin);
			prevAlmostWin = clipCounter;
			DialogueManager.source.player.clip = DialogueManager.source.almostWin [clipCounter];
			DialogueManager.source.player.Play ();
		}
	}

	public void PlayLose ()
	{
		if (DialogueManager.source.player.isPlaying)
			DialogueManager.source.player.Stop ();
		do {
			clipCounter = Random.Range (0, DialogueManager.source.lose.Count);
		} while(clipCounter == prevLose);
		DialogueManager.source.player.clip = DialogueManager.source.lose [clipCounter];
		DialogueManager.source.player.Play ();
	}

	public void PlayWin ()
	{
		if (DialogueManager.source.player.isPlaying)
			DialogueManager.source.player.Stop ();
		do {
			clipCounter = Random.Range (0, DialogueManager.source.win.Count - 1);
		} while(clipCounter == prevWin);
		prevWin = clipCounter;
		DialogueManager.source.player.clip = DialogueManager.source.win [clipCounter];
		DialogueManager.source.player.Play ();
	}

	public void PlayFinalWin ()
	{
		if (DialogueManager.source.player.isPlaying)
			DialogueManager.source.player.Stop ();
	
		DialogueManager.source.player.clip = DialogueManager.source.win [DialogueManager.source.win.Count-1];
		DialogueManager.source.player.Play ();
	}

}
