﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotChild : MonoBehaviour {

    public Transform cameraRig, headCam;
  public  Vector3 posDif;

	// Use this for initialization
	void Start () {
      //  headCam = Camera.main.gameObject.transform;
      //  Debug.Log(headCam.position +" + " +  transform.position + " = " +  (headCam.position - transform.position));
        posDif =  cameraRig.position - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = headCam.position - posDif;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, headCam.eulerAngles.y+90, transform.eulerAngles.z);
	}
}
